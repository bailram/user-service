package com.bailram.userservice.web;

import com.bailram.usermanagementservice.domain.dto.UserDto;
import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;
import com.bailram.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1")
public class UserServiceController {
    @Autowired
    private UserService userService;

    @GetMapping("/getAllUser")
    public UserResponse getAll() {
        return userService.getAllUserUrl(null);
    }

    @GetMapping("/getUserByRole")
    public UserResponse getByRole(@RequestBody UserRequest req) {
        return userService.getAllUserUrl(req);
    }

    @PostMapping("/add")
    public String addUser(@RequestBody UserDto req) {
        return userService.getAddUserUrl(req);
    }

    @PutMapping("/update")
    public String updateUser(@RequestBody UserDto req) {
        return userService.getUpdateUserUrl(req);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable long id) {
        return userService.getDeleteUserUrl(id);
    }
}
