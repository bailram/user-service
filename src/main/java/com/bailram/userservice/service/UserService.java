package com.bailram.userservice.service;

import com.bailram.usermanagementservice.domain.dto.UserDto;
import com.bailram.usermanagementservice.domain.dto.UserRequest;
import com.bailram.usermanagementservice.domain.dto.UserResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Log4j2
@Service
public class UserService {
    @Autowired
    private RestTemplate restTemplate;

    @Value("${getAllUserUrl}")
    private String getAllUserUrl;
    @Value("${getUserByRoleUrl}")
    private String getUserByRoleUrl;
    @Value("${addUserUrl}")
    private String addUserUrl;
    @Value("${updateUserUrl}")
    private String updateUserUrl;
    @Value("${deleteUserUrl}")
    private String deleteUserUrl;

    public UserResponse getAllUserUrl(UserRequest request) {
        return restTemplate.postForObject(getAllUserUrl, request, UserResponse.class);
    }

    public String getAddUserUrl(UserDto request) {
        return restTemplate.postForObject(addUserUrl, request, String.class);
    }

    public String getUpdateUserUrl(UserDto request) {
        return restTemplate.postForObject(updateUserUrl, request, String.class);
    }

    public String getDeleteUserUrl(long id) {
        return restTemplate.postForObject(deleteUserUrl+id, null, String.class);
    }
}
